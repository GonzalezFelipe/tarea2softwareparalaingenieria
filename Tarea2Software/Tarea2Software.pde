//Necesario instalar la libreria Sound
import processing.sound.*; 
int x;
int y;
int x1;
int y1;
int count;
float pos;
float vel;
float ace;
SoundFile sonido;

void setup(){
  size(700,700);
  x=250;
  y=250;
  x1=550;
  y1=300;
  vel=0;
  ace=0;
  pos=0;
  count=0;
  sonido=new SoundFile(this, "Beep.mp3");
}

void draw(){
  background(0);
  float aux=map(vel,0,4.6,0,200);
  float aux2=map(ace, -0.04, 0.04, 2, -2);
  ////Figuras del Marcador////
  //Velocimetro
  fill(0);
  strokeWeight(2);
  stroke(255);
  ellipse(x,y,300,300);
  arc(x,y,200,200,3*PI/4, 9*PI/4);
  strokeWeight(4);
  fill(0);
  ellipse(x,y-100,5,5);
  ellipse(x-100,y,5,5);
  ellipse(x+100,y,5,5);
  ellipse(x-50*sqrt(2),y-50*sqrt(2),5,5);
  ellipse(x-50*sqrt(2),y+50*sqrt(2),5,5);
  ellipse(x+50*sqrt(2),y-50*sqrt(2),5,5);
  ellipse(x+50*sqrt(2),y+50*sqrt(2),5,5);
  stroke(255);
  rect(x-50,y+80,100,50);
  //Acelerometro
  fill(0);
  strokeWeight(2);
  stroke(255);
  ellipse(x1,y1,200,200);
  arc(x1,y1,100, 100, PI, 2*PI);
  fill(255);
  ellipse(x1,y1,20,20);
  strokeWeight(4);
  line(x1, y1, x1+cos(aux2-6*PI/4)*30, y1-10+sin(aux2-10*PI/4)*30);
  fill(0);
  ellipse(x1,y1-50,5,5);
  ellipse(x1-50,y1,5,5);
  ellipse(x1+50,y1,5,5);
  ellipse(x1-25*sqrt(2),y1-25*sqrt(2),5,5);
  ellipse(x1+25*sqrt(2),y1-25*sqrt(2),5,5);
  stroke(255);
  rect(x1-40, y1+30, 80, 40);
  //
  stroke(255);
  line(0,590,700,590);
  ////Botones y leds////
  fill(255);
  stroke(0);
  rect(10,600,180,90);
  rect(210,600,180,90);
  fill(100,0,0);
  ellipse(650,650,30,30);
  ////Textos////
  fill(0);
  textSize(30);
  text("ACELERADOR", 15,660);
  text("FRENO", 260,660);
  fill(255);
  textSize(20);
  text("0" ,x-20-50*sqrt(2),y+10+50*sqrt(2));
  text("33.3" ,x-40-100,y+5);
  text("66.6" ,x-45-50*sqrt(2),y+5-50*sqrt(2));
  text("100" ,x-15,y-100-10);
  text("133.3" ,x+5+50*sqrt(2),y+5-50*sqrt(2));
  text("166.6" ,x+5+100,y+5);
  text("200" ,x+5+50*sqrt(2),y+10+50*sqrt(2));
  textSize(25);
  text(aux ,x-48,y+115);
  text(ace, x1-33, y1+60);
  textSize(30);
  text("0", x1-5, y1-70);
  text("-", x1-80, y1);
  text("+", x1+70, y1);
  text("Velocidad", x-60,y+200);
  text("Aceleración", x1-70, y1+150);
  ////Funciones//// 
  //Movimiento
  pos=pos+vel;
  vel=vel+ace;
  //Funcionamiento de botones y alarmas
  if(mouseY>599 && mouseY<689 && mouseX<189 && mouseX>9){
    ace=ace+0.00002;
    count++;
    fill(100,0,0);
    ellipse(650,650,30,30);
    if (count>600){
      fill(255);
      text("Por favor, suelte", 450, 630);
      text("el acelerador", 450, 670);
      fill(255,0,0);
      ellipse(650,650,30,30);
      if (sonido.isPlaying()){
      }
      else{ 
        sonido.play();
      }
    }
  }
  else if(mouseY>599 && mouseY<689 && mouseX<389 && mouseX>209){
    ace=ace-0.00002;
    count=0;
    fill(100,0,0);
    ellipse(650,650,30,30);
    fill(255,0,0);
    rect(210,600,180,90);
    fill(0);
    textSize(30);
    text("FRENO", 260,660);
  }
  else{
    ace=ace-0.00001;
    count=0;
    fill(100,0,0);
    ellipse(650,650,30,30); 
  }
  //Limites de valores
  if (vel<0){
    vel=0;
    ace=0;
  }
  if (vel>4.6){
    vel=4.6;
    ace=0;
  }
  //Funcion que da color según la posición a la aguja del velocimetro
  if (aux<100){
    stroke(0,255,0);
    line(x, y, x+cos(vel-5*PI/4)*90, y+sin(vel-5*PI/4)*90);
    fill(0,255,0);
    ellipse(x,y,30,30);
  }
  else if (aux<170 && aux>=100){
    stroke(255,255,0);
    line(x, y, x+cos(vel-5*PI/4)*90, y+sin(vel-5*PI/4)*90);
    fill(255,255,0);
    ellipse(x,y,30,30);
  }
  else if (aux>=170){
    stroke(255,0,0);
    line(x, y, x+cos(vel-5*PI/4)*90, y+sin(vel-5*PI/4)*90);
    fill(255,0,0);
    ellipse(x,y,30,30);
  }
}
